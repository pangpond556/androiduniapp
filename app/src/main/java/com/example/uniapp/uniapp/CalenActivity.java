package com.example.uniapp.uniapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

public class CalenActivity extends AppCompatActivity {
    public static TextView api;
    private WebView mWebView;
        /*api = findViewById(R.id.api);

        fetchData process = new fetchData();
        process.execute();*/

    @Override
        protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calen);
        mWebView = (WebView) findViewById(R.id.webView);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setWebChromeClient(new WebChromeClient());
        mWebView.setWebViewClient(new WebViewClient());
        mWebView.loadUrl("https://www3.reg.cmu.ac.th/regist160/public");
    }

    @Override
    public void onBackPressed() {
        if (mWebView.canGoBack()) {
            mWebView.goBack();
        } else {
            finish();
        }
    }

    /**
     * Start Main Activity
     */

    public void StartMainAct(View view) {
        Intent main = new Intent(this, MainActivity.class);
        startActivity(main);
        finish();
    }

    /**
     * Start Map Activity
     */
    public void StartMapAct(View view) {
        Intent map = new Intent(this, MapsActivity.class);
        startActivity(map);
        finish();
    }

    /**
     * Go Profile
     */
    public void StartProfile(View view) {
        Intent profile = new Intent(this, LoginActivity.class);
        startActivity(profile);
        finish();
    }

    /**
     * Go Profile
     */
    public void StartCalen(View view) {
        Intent calen = new Intent(this, CalenActivity.class);
        startActivity(calen);
        finish();
    }
}
