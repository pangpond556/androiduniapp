package com.example.uniapp.uniapp;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by TK on 14/12/2017.
 */

public class News implements Parcelable {
    private String id, title, time, tag, detail;
    private int category;

    public News(JSONObject object) throws JSONException {
        this.id = object.getString("id");
        this.title = object.getString("title");
        this.time = object.getString("time");
        this.tag = object.getString("tag");
        this.detail = object.getString("detail");
        this.category = object.getInt("category");
    }

    protected News(Parcel in) {
        id = in.readString();
        title = in.readString();
        time = in.readString();
        tag = in.readString();
        detail = in.readString();
        category = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(time);
        dest.writeString(tag);
        dest.writeString(detail);
        dest.writeInt(category);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<News> CREATOR = new Creator<News>() {
        @Override
        public News createFromParcel(Parcel in) {
            return new News(in);
        }

        @Override
        public News[] newArray(int size) {
            return new News[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }


}
