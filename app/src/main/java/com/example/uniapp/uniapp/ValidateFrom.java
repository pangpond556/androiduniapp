package com.example.uniapp.uniapp;

import android.os.AsyncTask;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by neko on 22/11/2560.
 */

public class ValidateFrom extends AsyncTask<String, String, String> {

    public ValidateFrom(String email) {
        //set context variables if required
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }


    @Override
    protected String doInBackground(String... params) {

        String urlString = params[0]; // URL to call
        String data = params[1]; //data to post
        OutputStream out = null;
        try {

            URL url = new URL("http://192.168.0.171/se_api/test");
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            out = new BufferedOutputStream(urlConnection.getOutputStream());

            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
            writer.write(data);
            writer.flush();
            writer.close();
            out.close();
            urlConnection.connect();


        } catch (Exception e) {

            System.out.println(e.getMessage());


        }

        return urlString;
    }



}


