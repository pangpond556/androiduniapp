package com.example.uniapp.uniapp;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.EncryptedPrivateKeyInfo;

/**
 * Created by neko on 22/11/2560.
 */


public class fetchData extends AsyncTask<Void,Void,Void> {
    String data = "";
    String dataParsed = "";
    String singleParsed = "";
    private List<News> newsList = new ArrayList<>();
    private List<News> educationList = new ArrayList<>();
    private List<News> calendarList = new ArrayList<>();

    public SendResponse response;
    public interface SendResponse {
        void onFinish(List<News> newsList, List<News> educationList, List<News> calendarList);
    }

    public fetchData(SendResponse response) {
        this.response = response;
    }


    //    TODO: Deal with fetch & decode jason file form url.
    @Override
    protected Void doInBackground(Void... voids) {
        try {
            URL url = new URL("http://chromauniapp.ddns.net:3000/news");
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = httpURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            while (line != null){
                line = bufferedReader.readLine();
                data = data + line;
            }
//TODO : old JSON Encrypted

            JSONArray JA = new JSONArray(data);

            List<String> filtered = new ArrayList<>();
            JSONArray ary = new JSONArray(data);

            for (int i = 0; i < ary.length(); i++) {
                switch (ary.getJSONObject(i).getInt("category")) {
                    case 1:
                        newsList.add(new News(ary.getJSONObject(i)));
                        break;
                    case 2:
                        educationList.add(new News(ary.getJSONObject(i)));
                        break;
                    case 3:
                        calendarList.add(new News(ary.getJSONObject(i)));
                        break;
                }
            }


        } catch (IOException|JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        response.onFinish(newsList, educationList, calendarList );
    }
}
