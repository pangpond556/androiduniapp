package com.example.uniapp.uniapp;

import android.Manifest;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private GoogleMap mMap;
    private GoogleApiClient client;
    private LocationRequest locationRequest;
    private Location lastLocation;
    private Marker currentLocationMarker;
    public static final int REQUEST_LOCATION_CODE = 99;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            checkLocationPermission();
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION_CODE:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    //permission is granted
                    if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
                    {
                        if(client == null)
                        {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }
                }
                else
                {
                    Toast.makeText(this, "Permission Denied!" , Toast.LENGTH_LONG).show();
                }
                return;
        }
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

//        TODO : Location of Bus station
        genMark();

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) ==  PackageManager.PERMISSION_GRANTED) {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
            LatLng myLocation = new LatLng(18.795339, 98.951637);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(myLocation));
        }
    }

    protected synchronized void buildGoogleApiClient() {
        client = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        client.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        lastLocation = location;

        if (currentLocationMarker != null) {
            currentLocationMarker.remove();
        }

        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Location");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));

        currentLocationMarker = mMap.addMarker(markerOptions);

        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomBy(10));

        if (client != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(client, this);
        }
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        locationRequest = new LocationRequest();

        locationRequest.setInterval(1000);
        locationRequest.setFastestInterval(1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(client, locationRequest, this);
        }
    }

    public boolean checkLocationPermission() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION_CODE);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION_CODE);
            }
            return false;

        } else
            return true;

    }


    /**
     * Start Main Activity
     */
    public void StartMainAct(View view) {
        Intent main = new Intent(this, MainActivity.class);
        startActivity(main);
        finish();
    }

    /**
     * Start Map Activity
     */
    public void StartMapAct(View view) {
        Intent map = new Intent(this, MapsActivity.class);
//        startActivity(map);
//        finish();
    }

    /**
     * Go Profile
     */
    public void StartProfile(View view) {
        Intent profile = new Intent(this, LoginActivity.class);
        startActivity(profile);
        finish();
    }

    /**
     * Go Profile
     */
    public void StartCalen(View view) {
        Intent profile = new Intent(this, CalenActivity.class);
        startActivity(profile);
        finish();
    }


    public void genMark(){

        BitmapDescriptor o1 = BitmapDescriptorFactory.fromResource(R.drawable.o01);
        BitmapDescriptor o2 = BitmapDescriptorFactory.fromResource(R.drawable.o02);
        BitmapDescriptor o3 = BitmapDescriptorFactory.fromResource(R.drawable.o03);
        BitmapDescriptor o4 = BitmapDescriptorFactory.fromResource(R.drawable.o04);
        BitmapDescriptor o5 = BitmapDescriptorFactory.fromResource(R.drawable.o05);
        BitmapDescriptor o6 = BitmapDescriptorFactory.fromResource(R.drawable.o06);
        BitmapDescriptor o7 = BitmapDescriptorFactory.fromResource(R.drawable.o07);
        BitmapDescriptor o8 = BitmapDescriptorFactory.fromResource(R.drawable.o08);
        BitmapDescriptor o9 = BitmapDescriptorFactory.fromResource(R.drawable.o09);
        BitmapDescriptor o10 = BitmapDescriptorFactory.fromResource(R.drawable.o10);
        BitmapDescriptor g1 = BitmapDescriptorFactory.fromResource(R.drawable.g01);
        BitmapDescriptor g2 = BitmapDescriptorFactory.fromResource(R.drawable.g02);
        BitmapDescriptor g3 = BitmapDescriptorFactory.fromResource(R.drawable.g03);
        BitmapDescriptor g4 = BitmapDescriptorFactory.fromResource(R.drawable.g04);
        BitmapDescriptor g5 = BitmapDescriptorFactory.fromResource(R.drawable.g05);
        BitmapDescriptor g6 = BitmapDescriptorFactory.fromResource(R.drawable.g06);
        BitmapDescriptor g7 = BitmapDescriptorFactory.fromResource(R.drawable.g07);
        BitmapDescriptor g8 = BitmapDescriptorFactory.fromResource(R.drawable.g08);
        BitmapDescriptor g9 = BitmapDescriptorFactory.fromResource(R.drawable.g09);
        BitmapDescriptor g10 = BitmapDescriptorFactory.fromResource(R.drawable.g10);
        BitmapDescriptor g11 = BitmapDescriptorFactory.fromResource(R.drawable.g11);
        BitmapDescriptor g12 = BitmapDescriptorFactory.fromResource(R.drawable.g12);
        BitmapDescriptor g13 = BitmapDescriptorFactory.fromResource(R.drawable.g13);
        BitmapDescriptor g14 = BitmapDescriptorFactory.fromResource(R.drawable.g14);
        BitmapDescriptor g15 = BitmapDescriptorFactory.fromResource(R.drawable.g15);
        BitmapDescriptor g16 = BitmapDescriptorFactory.fromResource(R.drawable.g16);
        BitmapDescriptor g17 = BitmapDescriptorFactory.fromResource(R.drawable.g17);
        BitmapDescriptor v1 = BitmapDescriptorFactory.fromResource(R.drawable.v01);
        BitmapDescriptor v2 = BitmapDescriptorFactory.fromResource(R.drawable.v02);
        BitmapDescriptor v3 = BitmapDescriptorFactory.fromResource(R.drawable.v03);
        BitmapDescriptor v4 = BitmapDescriptorFactory.fromResource(R.drawable.v04);
        BitmapDescriptor v5 = BitmapDescriptorFactory.fromResource(R.drawable.v05);
        BitmapDescriptor v6 = BitmapDescriptorFactory.fromResource(R.drawable.v06);
        BitmapDescriptor v7 = BitmapDescriptorFactory.fromResource(R.drawable.v07);
        BitmapDescriptor v8 = BitmapDescriptorFactory.fromResource(R.drawable.v08);
        BitmapDescriptor v9 = BitmapDescriptorFactory.fromResource(R.drawable.v09);
        BitmapDescriptor v10 = BitmapDescriptorFactory.fromResource(R.drawable.v10);
        BitmapDescriptor v11 = BitmapDescriptorFactory.fromResource(R.drawable.v11);
        BitmapDescriptor v12 = BitmapDescriptorFactory.fromResource(R.drawable.v12);
        BitmapDescriptor v13 = BitmapDescriptorFactory.fromResource(R.drawable.v13);
        BitmapDescriptor v14 = BitmapDescriptorFactory.fromResource(R.drawable.v13);
        BitmapDescriptor r1 = BitmapDescriptorFactory.fromResource(R.drawable.r01);
        BitmapDescriptor r2 = BitmapDescriptorFactory.fromResource(R.drawable.r02);
        BitmapDescriptor r3 = BitmapDescriptorFactory.fromResource(R.drawable.r03);
        BitmapDescriptor r4 = BitmapDescriptorFactory.fromResource(R.drawable.r04);
        BitmapDescriptor r5 = BitmapDescriptorFactory.fromResource(R.drawable.r05);
        BitmapDescriptor r6 = BitmapDescriptorFactory.fromResource(R.drawable.r06);
        BitmapDescriptor r7 = BitmapDescriptorFactory.fromResource(R.drawable.r07);
        BitmapDescriptor r8 = BitmapDescriptorFactory.fromResource(R.drawable.r08);
        BitmapDescriptor r9 = BitmapDescriptorFactory.fromResource(R.drawable.r09);
        BitmapDescriptor b1 = BitmapDescriptorFactory.fromResource(R.drawable.b01);
        BitmapDescriptor b2 = BitmapDescriptorFactory.fromResource(R.drawable.b02);
        BitmapDescriptor b3 = BitmapDescriptorFactory.fromResource(R.drawable.b03);
        BitmapDescriptor b4 = BitmapDescriptorFactory.fromResource(R.drawable.b04);
        BitmapDescriptor b5 = BitmapDescriptorFactory.fromResource(R.drawable.b05);
        BitmapDescriptor b6 = BitmapDescriptorFactory.fromResource(R.drawable.b06);
        BitmapDescriptor b7 = BitmapDescriptorFactory.fromResource(R.drawable.b07);
        BitmapDescriptor b8 = BitmapDescriptorFactory.fromResource(R.drawable.b08);
        BitmapDescriptor b9 = BitmapDescriptorFactory.fromResource(R.drawable.b09);
        BitmapDescriptor b10 = BitmapDescriptorFactory.fromResource(R.drawable.b10);
        BitmapDescriptor b11 = BitmapDescriptorFactory.fromResource(R.drawable.b11);
        BitmapDescriptor b12 = BitmapDescriptorFactory.fromResource(R.drawable.b12);
        BitmapDescriptor w1 = BitmapDescriptorFactory.fromResource(R.drawable.w01);
        BitmapDescriptor w2 = BitmapDescriptorFactory.fromResource(R.drawable.w02);
        BitmapDescriptor w3 = BitmapDescriptorFactory.fromResource(R.drawable.w03);
        BitmapDescriptor w4 = BitmapDescriptorFactory.fromResource(R.drawable.w04);
        BitmapDescriptor w5 = BitmapDescriptorFactory.fromResource(R.drawable.w05);
        BitmapDescriptor w6 = BitmapDescriptorFactory.fromResource(R.drawable.w06);
        BitmapDescriptor w7 = BitmapDescriptorFactory.fromResource(R.drawable.w07);
        BitmapDescriptor w8 = BitmapDescriptorFactory.fromResource(R.drawable.w08);
        BitmapDescriptor w9 = BitmapDescriptorFactory.fromResource(R.drawable.w09);



//TODO: Orange Line

        mMap.addMarker(new MarkerOptions()
                .icon(o1)
                .draggable(true)
                .position(new LatLng(18.799662, 98.948616))
                .title("จุด 1")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(o2)
                .draggable(true)
                .position(new LatLng(18.796657, 98.952945))
                .title("จุด 2")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(o3)
                .draggable(true)
                .position(new LatLng(18.796278, 98.955480))
                .title("จุด 3")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(o4)
                .draggable(true)
                .position(new LatLng(18.796093, 98.959768))
                .title("จุด 4")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(o5)
                .draggable(true)
                .position(new LatLng(18.798636, 98.958366))
                .title("จุด 5")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(o6)
                .draggable(true)
                .position(new LatLng(18.798865, 98.957406))
                .title("จุด 6")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(o7)
                .draggable(true)
                .position(new LatLng(18.799515, 98.955998))
                .title("จุด 7")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(o8)
                .draggable(true)
                .position(new LatLng(18.799505, 98.954120))
                .title("จุด 8")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(o9)
                .draggable(true)
                .position(new LatLng(18.799421, 98.952782))
                .title("จุด 9")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(o10)
                .draggable(true)
                .position(new LatLng(18.798907, 98.951554))
                .title("จุด 10")
                .snippet("ที่รอรถ"));

//        TODO: Green Line

        mMap.addMarker(new MarkerOptions()
                .icon(g1)
                .draggable(true)
                .position(new LatLng(18.807532, 98.954828))
                .title("จุด 1")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(g2)
                .draggable(true)
                .position(new LatLng(18.804762, 98.954387))
                .title("จุด 2")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(g3)
                .draggable(true)
                .position(new LatLng(18.803693, 98.953212))
                .title("จุด 3")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(g4)
                .draggable(true)
                .position(new LatLng(18.801533, 98.951302))
                .title("จุด 4")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(g5)
                .draggable(true)
                .position(new LatLng(18.799536, 98.952205))
                .title("จุด 5")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(g6)
                .draggable(true)
                .position(new LatLng(18.798782, 98.953375))
                .title("จุด 6")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(g7)
                .draggable(true)
                .position(new LatLng(18.796818, 98.953401))
                .title("จุด 7")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(g8)
                .draggable(true)
                .position(new LatLng(18.795426, 98.954285))
                .title("จุด 8")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(g9)
                .draggable(true)
                .position(new LatLng(18.793577, 98.955223))
                .title("จุด 9")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(g10)
                .draggable(true)
                .position(new LatLng(18.793387, 98.955263))
                .title("จุด 10")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(g11)
                .draggable(true)
                .position(new LatLng(18.795825, 98.954038))
                .title("จุด 11")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(g12)
                .draggable(true)
                .position(new LatLng(18.797688, 98.953238))
                .title("จุด 12")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(g13)
                .draggable(true)
                .position(new LatLng(18.799410, 98.952827))
                .title("จุด 13")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(g14)
                .draggable(true)
                .position(new LatLng(18.801308, 98.951152))
                .title("จุด 14")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(g15)
                .draggable(true)
                .position(new LatLng(18.803934, 98.953167))
                .title("จุด 15")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(g16)
                .draggable(true)
                .position(new LatLng(18.804599, 98.953996))
                .title("จุด 16")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(g17)
                .draggable(true)
                .position(new LatLng(18.805803, 98.955407))
                .title("จุด 17")
                .snippet("ที่รอรถ"));

        //        TODO: Violet Line

        mMap.addMarker(new MarkerOptions()
                .icon(v1)
                .draggable(true)
                .position(new LatLng(18.799417, 98.952800))
                .title("จุด 1")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(v2)
                .draggable(true)
                .position(new LatLng(18.798833, 98.953401))
                .title("จุด 2")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(w3)
                .draggable(true)
                .position(new LatLng(18.796845, 98.953404))
                .title("จุด 3")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(w4)
                .draggable(true)
                .position(new LatLng(18.795488, 98.954275))
                .title("จุด 4")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(v5)
                .draggable(true)
                .position(new LatLng(18.793577, 98.955252))
                .title("จุด 5")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(v6)
                .draggable(true)
                .position(new LatLng(18.792637, 98.956750))
                .title("จุด 6")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(v7)
                .draggable(true)
                .position(new LatLng(18.793152, 98.957030))
                .title("จุด 7")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(v8)
                .draggable(true)
                .position(new LatLng(18.793152, 98.957030))
                .title("จุด 8")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(v9)
                .draggable(true)
                .position(new LatLng(18.794749, 98.958703))
                .title("จุด 9")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(v10)
                .draggable(true)
                .position(new LatLng(18.797131, 98.957147))
                .title("จุด 10")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(v11)
                .draggable(true)
                .position(new LatLng(18.798927, 98.957432))
                .title("จุด 11")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(v12)
                .draggable(true)
                .position(new LatLng(18.799516, 98.956027))
                .title("จุด 12")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(v13)
                .draggable(true)
                .position(new LatLng(18.801485, 98.956716))
                .title("จุด 13")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(v14)
                .draggable(true)
                .position(new LatLng(18.799488, 98.954124))
                .title("จุด 14")
                .snippet("ที่รอรถ"));

        //        TODO: W Line

        mMap.addMarker(new MarkerOptions()
                .icon(w1)
                .draggable(true)
                .position(new LatLng(18.807526, 98.954783))
                .title("จุด 1")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(w2)
                .draggable(true)
                .position(new LatLng(18.804808, 98.954423))
                .title("จุด 2")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(w3)
                .draggable(true)
                .position(new LatLng(18.804808, 98.954423))
                .title("จุด 3")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(w4)
                .draggable(true)
                .position(new LatLng(18.800703, 98.953630))
                .title("จุด 4")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(w5)
                .draggable(true)
                .position(new LatLng(18.798842, 98.958747))
                .title("จุด 5")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(w6)
                .draggable(true)
                .position(new LatLng(18.799520, 98.956012))
                .title("จุด 6")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(w7)
                .draggable(true)
                .position(new LatLng(18.800488, 98.953307))
                .title("จุด 7")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(w8)
                .draggable(true)
                .position(new LatLng(18.803003, 98.954234))
                .title("จุด 8")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(w9)
                .draggable(true)
                .position(new LatLng(18.804563, 98.954014))
                .title("จุด 9")
                .snippet("ที่รอรถ"));

        //        TODO: Blue Line

        mMap.addMarker(new MarkerOptions()
                .icon(b1)
                .draggable(true)
                .position(new LatLng(18.799414, 98.952823))
                .title("จุด 1")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(b2)
                .draggable(true)
                .position(new LatLng(18.801172, 98.950446))
                .title("จุด 2")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(b3)
                .draggable(true)
                .position(new LatLng(18.802198, 98.948665))
                .title("จุด 3")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(b4)
                .draggable(true)
                .position(new LatLng(18.801389, 98.951077))
                .title("จุด 4")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(b5)
                .draggable(true)
                .position(new LatLng(18.803071, 98.950059))
                .title("จุด 5")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(b6)
                .draggable(true)
                .position(new LatLng(18.803809, 98.949044))
                .title("จุด 6")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(b7)
                .draggable(true)
                .position(new LatLng(18.804469, 98.947389))
                .title("จุด 7")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(b8)
                .draggable(true)
                .position(new LatLng(18.801664, 98.951318))
                .title("จุด 8")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(b9)
                .draggable(true)
                .position(new LatLng(18.798927, 98.951574))
                .title("จุด 9")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(b10)
                .draggable(true)
                .position(new LatLng(18.798104, 98.948392))
                .title("จุด 10")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(b11)
                .draggable(true)
                .position(new LatLng(18.796647, 98.953016))
                .title("จุด 11")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(b12)
                .draggable(true)
                .position(new LatLng(18.797863, 98.953180))
                .title("จุด 12")
                .snippet("ที่รอรถ"));

        //        TODO: Blue Line

        mMap.addMarker(new MarkerOptions()
                .icon(r1)
                .draggable(true)
                .position(new LatLng(18.799424, 98.952814))
                .title("จุด 1")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(r2)
                .draggable(true)
                .position(new LatLng(18.801414, 98.951095))
                .title("จุด 2")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(r3)
                .draggable(true)
                .position(new LatLng(18.803063, 98.950088))
                .title("จุด 3")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(r4)
                .draggable(true)
                .position(new LatLng(18.803962, 98.948963))
                .title("จุด 4")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(r5)
                .draggable(true)
                .position(new LatLng(18.805302, 98.950185))
                .title("จุด 5")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(r6)
                .draggable(true)
                .position(new LatLng(18.804684, 98.951694))
                .title("จุด 6")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(r7)
                .draggable(true)
                .position(new LatLng(18.803892, 98.953214))
                .title("จุด 7")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(r8)
                .draggable(true)
                .position(new LatLng(18.803008, 98.954416))
                .title("จุด 8")
                .snippet("ที่รอรถ"));

        mMap.addMarker(new MarkerOptions()
                .icon(r9)
                .draggable(true)
                .position(new LatLng(18.800755, 98.953627))
                .title("จุด 9")
                .snippet("ที่รอรถ"));
    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
