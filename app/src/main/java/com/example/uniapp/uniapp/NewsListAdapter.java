package com.example.uniapp.uniapp;

import android.graphics.Color;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TK on 14/12/2017.
 */

public class NewsListAdapter extends RecyclerView.Adapter<NewsListAdapter.ViewHolder> {

    private List<News> newsList = new ArrayList<>();

    public void addAll(List<News> newsList) {
        this.newsList = newsList;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_news_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindTo(newsList.get(position));
        if(position % 2 == 0) {
            holder.changeColor();
        }
    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private AppCompatTextView title, time, detail;
        private LinearLayout layout;
        ViewHolder(View itemView) {
            super(itemView);
            layout = itemView.findViewById(R.id.layout_main);
            title = itemView.findViewById(R.id.text_title);
            time = itemView.findViewById(R.id.text_time);
            detail = itemView.findViewById(R.id.text_detail);
        }

        void bindTo(News newsItem) {
            title.setText(newsItem.getTitle());
            time.setText(newsItem.getTime());
            detail.setText(Html.fromHtml(newsItem.getDetail()));
        }

        void changeColor() {
            layout.setBackgroundColor(Color.parseColor("#f5f1f9"));
        }
    }
}
