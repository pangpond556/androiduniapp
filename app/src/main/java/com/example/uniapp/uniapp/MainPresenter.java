package com.example.uniapp.uniapp;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static com.example.uniapp.uniapp.MainActivity.data;

/**
 * Created by TK on 14/12/2017.
 */

public class MainPresenter implements fetchData.SendResponse {

    private MainActivity activity;

    public MainPresenter() {

    }

    public void setView(MainActivity activity) {
        this.activity = activity;
        execute();
    }

    private void execute() {
        fetchData process = new fetchData(this);
        process.execute();
    }

    @Override
    public void onFinish(List<News> newsList, List<News> educationList, List<News> calendarList) {
        activity.setData(newsList, educationList, calendarList);
    }
}
