package com.example.uniapp.uniapp;

import android.content.Intent;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TabHost;
import android.widget.TextView;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import java.util.TimerTask;

public class MainActivity extends AppCompatActivity{

    ViewPager viewPager;
    TabHost tabHost;
    //    TODO: fetch data to dataObject
    public static TextView data;
    public static TextView rawdata;

    private MainPresenter presenter;
    private LinearLayoutManager layoutManager;
    private RecyclerView newsListView, educationListView, calendarListView;
    private NewsListAdapter newsListAdapter, educationAdapter, calendarAdapter;
    //public static TextView rawdata1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        presenter = new MainPresenter();
        presenter.setView(this);

        layoutManager = new LinearLayoutManager(this);
        newsListView = findViewById(R.id.recycler_news_list);
        newsListView.setLayoutManager(layoutManager);
        newsListView.setNestedScrollingEnabled(false);

        newsListAdapter = new NewsListAdapter();
        newsListView.setAdapter(newsListAdapter);

        educationListView = findViewById(R.id.recycler_education_list);
        educationListView.setLayoutManager(new LinearLayoutManager(this));
        educationAdapter = new NewsListAdapter();
        educationListView.setAdapter(educationAdapter);

        calendarListView = findViewById(R.id.recycler_calendar_list);
        calendarListView.setLayoutManager(new LinearLayoutManager(this));
        calendarAdapter = new NewsListAdapter();
        calendarListView.setAdapter(calendarAdapter);



        TabHost host = (TabHost)findViewById(R.id.tabHost);
        host.setup();

        //Tab 1
        TabHost.TabSpec spec = host.newTabSpec("Tab One");
        spec.setContent(R.id.tab1);
        spec.setIndicator("News");
        host.addTab(spec);

        //Tab 2
        spec = host.newTabSpec("Tab Two");
        spec.setContent(R.id.tab2);
        spec.setIndicator("Education");
        host.addTab(spec);

        //Tab 3
        spec = host.newTabSpec("Tab Three");
        spec.setContent(R.id.tab3);
        spec.setIndicator("Calendar");
        host.addTab(spec);




        final SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.sr);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false);
                        fetchData process = new fetchData(new fetchData.SendResponse() {
                            @Override
                            public void onFinish(List<News> newsList, List<News> eduList, List<News> calendarList) {
                                setData(newsList, eduList, calendarList);
                            }
                        });
                        process.execute();
                    }
                },0);
            }
        });


        viewPager = (ViewPager) findViewById(R.id.ViewPager);
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(this);
        viewPager.setAdapter(viewPagerAdapter);

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new MyTimerTask(), 2000, 4000);


//        rawdata = findViewById(R.id.rawdata);
        //rawdata1 = findViewById(R.id.rawdata1);

//        fetchData process = new fetchData();
//        process.execute();


    }

    /**
     * Start Main Activity
     */
    public void StartMainAct(View view) {
        Intent main = new Intent(this, MainActivity.class);
        startActivity(main);
        finish();
    }

    public void setData(List<News> data, List<News> eduData,List<News> calendardata) {
        newsListAdapter.addAll(data);
        educationAdapter.addAll(eduData);
        calendarAdapter.addAll(calendardata);
    }

    /**
     * Start Map Activity
     */
    public void StartMapAct(View view) {
        Intent map = new Intent(this, MapsActivity.class);
        startActivity(map);
        finish();
    }

    /**
     * Go Profile
     */
    public void StartProfile(View view) {
        Intent profile = new Intent(this, LoginActivity.class);
        startActivity(profile);
        finish();
    }

    /**
     * Go Profile
     */
    public void StartCalen(View view) {
        Intent profile = new Intent(this, CalenActivity.class);
        startActivity(profile);
        finish();
    }

    /**
     * Auto Images Slide
     */
    public class MyTimerTask extends TimerTask {
        @Override
        public void run() {
            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (viewPager.getCurrentItem() == 0) {
                        viewPager.setCurrentItem(1);
                    } else if (viewPager.getCurrentItem() == 1) {
                        viewPager.setCurrentItem(2);
                    } else {
                        viewPager.setCurrentItem(0);
                    }
                }
            });
        }
    }
}